#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "linheap.h"

#define BUFFER_SIZE (0x20000)

static uint8_t g_buffer[BUFFER_SIZE];

static struct linheap g_linheap = {
    .size = BUFFER_SIZE,
};

static bool IsMatch(const struct linheap* heap, size_t origin, size_t cur1, size_t cur2) {
    size_t heapOrigin = (size_t)(heap->origin - heap->buffer);
    size_t heapCur1 = (size_t)(heap->cur1 - heap->buffer);
    size_t heapCur2 = (size_t)(heap->cur2 - heap->buffer);
    return (heapOrigin == origin) && (heapCur1 == cur1) && (heapCur2 == cur2);
}

static void PrintState(const struct linheap* heap) {
    printf(" Size:    0x%lX\n", heap->size);
    printf(" Origin:  0x%lX\n", (size_t)(heap->origin - heap->buffer));
    printf(" Cur:     0x%05lX, 0x%05lX\n", (size_t)(heap->cur2 - heap->buffer), (size_t)(heap->cur1 - heap->buffer));
    if (heap->advance) {
        printf(" Advance: 0x%lX\n", (size_t)(heap->advance - heap->buffer));
    }
}

#define AllocAndVerify(A,B,C,D,E) AllocAndVerifyEx((A), (B), (C), (D), (E), __FILE__, __LINE__)

static void AllocAndVerifyEx(struct linheap* heap, size_t size, size_t expOrigin, size_t expCur1, size_t expCur2, const char *filename, size_t line) {
    void* ptr = linheap_alloc(heap, size);
    bool match = IsMatch(heap, expOrigin, expCur1, expCur2);
    const char* status = match ? "SUCCESS" : "FAILURE";
    if (ptr) {
        printf("[%s] Allocated space: 0x%05lX\n", status, size);
    }
    else {
        printf("[%s] Failed to allocate.\n", status);
    }
    PrintState(heap);
    if (!match) {
        printf("<%s>:%ld\n", filename, line);
        exit(1);
    }
    puts(""); // Newline
}

static void PrepareAndPrint(struct linheap* heap) {
    puts("Preparing for advance");
    linheap_prepare_advance(heap);
    PrintState(heap);
    puts(""); // Newline
}

static void FinishAndPrint(struct linheap* heap) {
    puts("Finishing advance");
    linheap_finish_advance(heap);
    PrintState(heap);
    puts(""); // Newline
}

static void RevertAndPrint(struct linheap* heap) {
    puts("Reverting advance");
    linheap_revert_advance(heap);
    PrintState(heap);
    puts(""); // Newline
}

static void SetHeap(struct linheap* heap, size_t origin, size_t cur1, size_t cur2) {
    linheap_clear(heap);
    heap->origin = heap->buffer + origin;
    heap->cur1 = heap->buffer + cur1;
    heap->cur2 = heap->buffer + cur2;
}

static void Test1(struct linheap* heap) {
    linheap_clear(heap);

    // Basic overall test of behavior.
    // Perform allocations to fill most of the heap buffer.
    AllocAndVerify(heap, 0xC000, 0, 0xC000, 0);
    AllocAndVerify(heap, 0x2000, 0, 0xE000, 0);
    AllocAndVerify(heap, 0x11000, 0, 0x1F000, 0);
    // Attempt allocation which will fail due to lack of available space.
    AllocAndVerify(heap, 0x3000, 0, 0x1F000, 0);

    // Prepare for advance with only 0x1000 heap space left at end.
    PrepareAndPrint(heap);
    // Then allocate last chunk of heap before finishing advance.
    AllocAndVerify(heap, 0x1000, 0, 0x20000, 0);
    FinishAndPrint(heap);
    // There is no space remaining at end of heap, but now there is free space at the start.
    AllocAndVerify(heap, 0x1000, 0x1F000, 0x20000, 0x1000);

    // Prepare for advance after allocating 0x1000 bytes at heap start.
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x3000, 0x1F000, 0x20000, 0x4000);
    // Finish advancing when advance pointer is before cur2, meaning cur1 takes cur2's place as cur2 is reset.
    FinishAndPrint(heap);

    // Allocate and verify that origin is now 0x1000 and cur2 is reset to 0.
    AllocAndVerify(heap, 0x1000, 0x1000, 0x5000, 0);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1000, 0x1000, 0x6000, 0);
    FinishAndPrint(heap);

    // Setup for testing non-cycleable allocation while preparing advance.
    AllocAndVerify(heap, 0x18000, 0x5000, 0x1E000, 0);
    AllocAndVerify(heap, 0x3000, 0x5000, 0x1E000, 0x3000);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1000, 0x5000, 0x1F000, 0x3000);
    FinishAndPrint(heap);

    // If not cycleable, cur2 should have reset to 0 after finishing advance.
    AllocAndVerify(heap, 0x1000, 0x1E000, 0x20000, 0);
    AllocAndVerify(heap, 0x4000, 0x1E000, 0x20000, 0x4000);
    // Ensure heap does not try to allocate chunk with data past origin.
    AllocAndVerify(heap, 0x1D000, 0x1E000, 0x20000, 0x4000);

    // Reset origin to prepare for testing cycleable allocation.
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1000, 0x1E000, 0x20000, 0x5000);
    FinishAndPrint(heap);
    AllocAndVerify(heap, 0x17000, 0x4000, 0x1C000, 0);

    // If cycleable, should cycle back to start as if contiguous buffer.
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x3000, 0x4000, 0x1F000, 0);
    AllocAndVerify(heap, 0x2000, 0x4000, 0x1F000, 0x2000);
    FinishAndPrint(heap);
    AllocAndVerify(heap, 0, 0x1C000, 0x1F000, 0x2000);

    // Should detect non-cycleable and only allocate using cur2 for most area.
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1000, 0x1C000, 0x1F000, 0x3000);
    // Advance pointer should be at 0x2000, ensure we can't allocate past origin.
    AllocAndVerify(heap, 0x19001, 0x1C000, 0x1F000, 0x3000);
    AllocAndVerify(heap, 0x19000, 0x1C000, 0x1F000, 0x1C000);
    // Should recognize non-cycleable and not allocate using cur1, even though it has enough space.
    AllocAndVerify(heap, 0x1000, 0x1C000, 0x1F000, 0x1C000);
    // Used cur2 for advance, cur1 should take its place and cur2 should be reset.
    FinishAndPrint(heap);
    AllocAndVerify(heap, 0, 0x2000, 0x1C000, 0);
}

static void Test2(struct linheap* heap) {
    linheap_clear(heap);

    // Test behavior which came up in production.
    AllocAndVerify(heap, 0, 0, 0, 0);
    // Test if cur1 == cur2 during prepare, but allocates before advance.
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1000, 0, 0x1000, 0);
    FinishAndPrint(heap);
    AllocAndVerify(heap, 0, 0, 0x1000, 0);
}

static void Test3(struct linheap* heap) {
    // Test a few more interesting cases.
    // Test cycleable and cur2 allocating all the way to origin pointer.
    linheap_clear(heap);
    AllocAndVerify(heap, 0x10000, 0, 0x10000, 0);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1000, 0, 0x11000, 0);
    FinishAndPrint(heap);
    PrepareAndPrint(heap);
    // Should allocate most of remaining space at cur1.
    AllocAndVerify(heap, 0xE000, 0x10000, 0x1F000, 0);
    // Heap is cycleable, should allocate at cur2 all the way to origin.
    AllocAndVerify(heap, 0x10000, 0x10000, 0x1F000, 0x10000);
    // Allocate a bit more at cur1.
    AllocAndVerify(heap, 0x800, 0x10000, 0x1F800, 0x10000);
    // Should fail to allocate any more.
    AllocAndVerify(heap, 0x1000, 0x10000, 0x1F800, 0x10000);
    FinishAndPrint(heap);

    // Test full heap before advance.
    linheap_clear(heap);
    AllocAndVerify(heap, 0x10000, 0, 0x10000, 0);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1000, 0, 0x11000, 0);
    FinishAndPrint(heap);
    // Fill up remaining space when not in advance.
    AllocAndVerify(heap, 0xE000, 0x10000, 0x1F000, 0);
    AllocAndVerify(heap, 0x10000, 0x10000, 0x1F000, 0x10000);
    AllocAndVerify(heap, 0x1000, 0x10000, 0x20000, 0x10000);
    // Should be unable to allocate before advance.
    AllocAndVerify(heap, 1, 0x10000, 0x20000, 0x10000);
    PrepareAndPrint(heap);
    // Should be unable to allocate during advance.
    AllocAndVerify(heap, 1, 0x10000, 0x20000, 0x10000);
    FinishAndPrint(heap);
    // Heap should be detected as empty after finish, and should be cleared.
    AllocAndVerify(heap, 0, 0, 0, 0);
}

static void Test4(struct linheap* heap) {
    // Test revert when origin is 0.
    linheap_clear(heap);
    AllocAndVerify(heap, 0, 0, 0, 0);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x4000, 0, 0x4000, 0);
    RevertAndPrint(heap);
    AllocAndVerify(heap, 0, 0, 0, 0);

    // Test revert when origin is 0 and full buffer.
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x20000, 0, 0x20000, 0);
    RevertAndPrint(heap);
    AllocAndVerify(heap, 0, 0, 0, 0);

    // Test revert when advancing with cur2.
    SetHeap(heap, 0x18000, 0x18000, 0x9000);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x3000, 0x18000, 0x18000, 0xC000);
    RevertAndPrint(heap);
    AllocAndVerify(heap, 0, 0x18000, 0x18000, 0x9000);

    // Test revert when advancing with cur1 and non-cycleable.
    SetHeap(heap, 0x14000, 0x1C000, 0x13000);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x4000, 0x14000, 0x20000, 0x13000);
    RevertAndPrint(heap);
    AllocAndVerify(heap, 0, 0x14000, 0x1C000, 0x13000);

    // Test revert when cycleable and heap is not full.
    SetHeap(heap, 0x1C000, 0x1C000, 0);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x5000, 0x1C000, 0x1C000, 0x5000);
    AllocAndVerify(heap, 0x2000, 0x1C000, 0x1E000, 0x5000);
    RevertAndPrint(heap);
    AllocAndVerify(heap, 0, 0x1C000, 0x1C000, 0);

    // Test revert when cycleable and heap is full.
    SetHeap(heap, 0x1C000, 0x1C000, 0);
    PrepareAndPrint(heap);
    AllocAndVerify(heap, 0x1C000, 0x1C000, 0x1C000, 0x1C000);
    AllocAndVerify(heap, 0x4000, 0x1C000, 0x20000, 0x1C000);
    // Should not be able to allocate further.
    AllocAndVerify(heap, 1, 0x1C000, 0x20000, 0x1C000);
    RevertAndPrint(heap);
    AllocAndVerify(heap, 0, 0x1C000, 0x1C000, 0);

    // Test revert when heap full and advance == origin == cur2.
    SetHeap(heap, 0x1C000, 0x20000, 0x1C000);
    PrepareAndPrint(heap);
    RevertAndPrint(heap);
    AllocAndVerify(heap, 0, 0x1C000, 0x20000, 0x1C000);
}

int main(int argc, char *argv[]) {
    // Initialize linheap structure.
    struct linheap* heap = &g_linheap;
    linheap_init(heap, g_buffer);

    Test1(heap);
    Test2(heap);
    Test3(heap);
    Test4(heap);

    return 0;
}
