#!/usr/bin/env python
# Script designed to take hex copy-paste (from PJ64 memory view) and output path node values, formatted as YAML list.

import fileinput
import io

hexranges = (tuple(x) for x in (range(0x30, 0x39 + 1), range(0x41, 0x46 + 1), range(0x61, 0x66 + 1)))
hexchars = sum(hexranges, ())

def filter_hex(input: str) -> str:
    """Filter hex characters from given input string."""
    encoded = input.encode('ascii')
    filtered = bytes(c for c in encoded if c in hexchars)
    result = filtered.decode('ascii')
    return result

def read_hex() -> str:
    """Read hex using `fileinput` library."""
    builder = io.StringIO()
    for line in fileinput.input():
        filtered = filter_hex(line)
        if len(filtered) == 0:
            return builder.getvalue()
        builder.write(filtered)
    return builder.getvalue()

def generate_hex_u16(input: str):
    """Parse input hex string as list of u16 integers."""
    if len(input) % 4 != 0:
        raise ValueError('Input hex not parseable as [u16].')
    count = int(len(input) / 4)
    for i in range(count):
        x = slice(i*4, (i+1)*4)
        curhex = input[x]
        value = int(curhex, 16)
        yield value

def generate_path_nodes(values):
    """Group items from iterable of u16 integers into pairs of 3."""
    if len(values) % 3 != 0:
        raise ValueError('Input values not parseable as path nodes.')
    count = int(len(values) / 3)
    for i in range(count):
        yield (values[i*3], values[(i*3)+1], values[(i*3)+2])

def build_path_nodes_string(nodes):
    """Build output string from path node values."""
    builder = io.StringIO()
    builder.write('[')
    for idx, node in enumerate(nodes):
        text = ', '.join('0x{0:04X}'.format(x) for x in node)
        builder.write(f'[{text}]')
        if (idx + 1) != len(nodes):
            builder.write(', ')
    builder.write(']')
    return builder.getvalue()

def main():
    """Main function."""
    input = read_hex()
    values = tuple(generate_hex_u16(input))
    nodes = tuple(generate_path_nodes(values))
    print(build_path_nodes_string(nodes))

if __name__ == '__main__':
    main()
