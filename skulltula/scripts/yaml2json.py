#!/usr/bin/env python

import argparse
import json
import sys
import yaml

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
    return parser

def main():
    args = get_parser().parse_args()
    data = args.infile.read()
    parsed = yaml.safe_load(data)
    string = json.dumps(parsed)
    print(string)

if __name__ == '__main__':
    main()
