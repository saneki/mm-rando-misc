import argparse
import re

pattern = re.compile(r'\(G_(.+)_(V?RAM) - G_\1_FILE\)')

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('files', nargs='+', help='files to replace in-place')
    return parser

def main():
    args = get_parser().parse_args()
    for path in args.files:
        print('Processing file: {}'.format(path))
        with open(path, 'r') as fp:
            text = fp.read()
        result = pattern.sub(r'G_\1_DELTA', text)
        with open(path, 'w') as fp:
            fp.write(result)

if __name__ == '__main__':
    main()
