#!/usr/bin/env python

# Hacky script for helping refactor old code style used for C structures with offsets, with offset comments on the right of fields.
# Moves offset comments to the left of fields, similar to the zeldaret mm project.

import argparse
import re

def fix(input: str) -> str:
    sections = tuple(x.strip() for x in input.split('/*'))
    if len(sections) == 1 or len(sections[0]) == 0:
        return input

    # Remove trailing comment end: "*/"
    comtext = sections[1][:-2]

    comment = tuple(x.strip() for x in comtext.split(","))
    if comment[0].startswith('0x'):
        value = int(comment[0], 16)
        hexval = '%X' % value
        fieldText = ' '.join(re.split(r'\s+', sections[0])).strip()
        if (input.startswith('}')):
            # Format line with structure full size.
            return "{0} // size = 0x{1}".format(fieldText, hexval)
        elif (len(comment) == 1):
            # Format field line with offset but no comment.
            return "    /* 0x{0} */ {1}".format(hexval, fieldText)
        else:
            # Format field line with offset and comment.
            return "    /* 0x{0} */ {1} // {2}".format(hexval, fieldText, comment[1])
    else:
        return input

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser

def fix_file(f):
    lines = f.readlines()
    for line in lines:
        print(fix(line).rstrip())

if __name__ == '__main__':
    args = get_parser().parse_args()
    with open(args.file) as f:
        fix_file(f)
