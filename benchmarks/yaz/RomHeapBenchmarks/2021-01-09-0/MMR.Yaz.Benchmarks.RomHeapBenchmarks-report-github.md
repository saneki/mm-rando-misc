``` ini

BenchmarkDotNet=v0.12.1, OS=Windows 10.0.19042
AMD Ryzen 7 1800X, 1 CPU, 16 logical and 8 physical cores
.NET Core SDK=5.0.101
  [Host]     : .NET Core 5.0.1 (CoreCLR 5.0.120.57516, CoreFX 5.0.120.57516), X64 RyuJIT
  DefaultJob : .NET Core 5.0.1 (CoreCLR 5.0.120.57516, CoreFX 5.0.120.57516), X64 RyuJIT


```
|    Method |        Mean |    Error |   StdDev |       Gen 0 |     Gen 1 |     Gen 2 | Allocated |
|---------- |------------:|---------:|---------:|------------:|----------:|----------:|----------:|
| DecodeMzx |    107.9 ms |  2.07 ms |  1.94 ms |  10200.0000 |         - |         - |  54.76 MB |
| DecodeNew |    105.6 ms |  0.50 ms |  0.45 ms |   5800.0000 |         - |         - |  34.17 MB |
| EncodeMzx | 14,643.8 ms | 84.55 ms | 74.95 ms | 171000.0000 | 8000.0000 | 2000.0000 | 842.39 MB |
| EncodeNew | 13,740.9 ms | 98.86 ms | 92.48 ms |  12000.0000 | 2000.0000 |         - |  72.63 MB |
