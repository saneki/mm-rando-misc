MM Rando Miscellaneous Repository
=================================

This repository contains miscellaneous code, scripts, and other things relating to [Majora's Mask Randomizer].

Current subprojects this repo contains:
- `linheap-testing`: Test code for revolving linear heap (`linheap`) structure. This is to be used as a heap for object data which is safe to use during and after room transitions, without risking the game's Display Lists pointing to potentially overwritten data.

[Majora's Mask Randomizer]:https://github.com/ZoeyZolotova/mm-rando
