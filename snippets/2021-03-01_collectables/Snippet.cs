/// Mostly provided by Zoey, randomizes GetItem table entries for collectables.
/// Place this somewhere in ItemSwapUtils.cs, and call in Builder::MakeROM after calling Builder::WriteItems?
public static void HackyRandomizeCollectables()
{
    int f = RomUtils.GetFileIndexForWriting(GET_ITEM_TABLE);
    var fileData = RomData.MMFileList[f].Data;
    var random = new System.Random();
    var itemPool = ItemUtils.AllLocations().Where(item => !item.IsRepeatable()).ToList();
    for (int getItemIndex = 0x1CC; getItemIndex <= 0x421; getItemIndex++)
    {
        // Get random item until it has a GetItem index.
        var item = itemPool.Random(random);
        while (!item.GetItemIndex().HasValue)
            item = itemPool.Random(random);

        var newItem = RomData.GetItemList[item.GetItemIndex().Value];
        var data = new byte[]
        {
            newItem.ItemGained,
            newItem.Flag,
            newItem.Index,
            newItem.Type,
            (byte)(newItem.Message >> 8),
            (byte)(newItem.Message & 0xFF),
            (byte)(newItem.Object >> 8),
            (byte)(newItem.Object & 0xFF),
        };
        int offset = (getItemIndex - 1) * 8;
        ReadWriteUtils.Arr_Insert(data, 0, data.Length, fileData, offset);
    }
}
